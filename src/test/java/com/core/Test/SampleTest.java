package com.core.Test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.core.Pages.GooglePage;
import com.core.base.BaseSetup;
import com.core.utility.FunctionsClass;

@Listeners(com.core.utility.TestListener.class)
public class SampleTest extends BaseSetup {
	WebDriver driver;

	@BeforeClass
	public void setup(ITestContext context) {
		try {
			driver = launchApplicationBrowser();
			context.setAttribute("WebDriver", driver);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Test
	public void sampleTestBrowserGoogle() {
		GooglePage.Instance(driver).launchURL("https://www.google.com");
		System.out.println("Driver info is: " + driver);
		GooglePage.Instance(driver).searchGoogle();
		Assert.assertTrue(true,"Result is not correct");
	}

	@Test
	public void sampleTestGmail() {
		GooglePage.Instance(driver).launchURL("https://www.google.com");
		GooglePage.Instance(driver).searchGoogle();
	}

	@Test
	public void readExcel() {
		try {
			String mobile = FunctionsClass.Instance().ExcelReader("Data", "MobileNumber").get(0);
			System.out.println(mobile);
			String name = FunctionsClass.Instance().ExcelReader("Data", "Name").get(0);
			System.out.println(name);
		} catch (Exception e) {
			logger.error(e);
		}
	}

}
