package com.core.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Java8Practice {
	public static void main(String args[]) {

		// Print single name - Find particular name
		List<String> names = Arrays.asList("John", "Prerit", "Ram", "Shyam");
		Optional<String> person = names.stream().filter(x -> x.contentEquals("Prerit")).findFirst();
		System.out.println("Matching name is: ");
		person.ifPresent(System.out::println);

		// List of Elements- print name more than 4 length
		List<String> perssons = names.stream().filter(x -> x.length() > 4).collect(Collectors.toList());
		System.out.println("Name whose length is more than 4: ");
		for (String name : perssons) {
			System.out.println(name);
		}

		// List of Integers - Remove duplicates
		List<Integer> numbers = Arrays.asList(1, 4, 3, 2, 5, 1, 2, 5, 2);
		List<Integer> newList = numbers.stream().distinct().collect(Collectors.toList());
		System.out.println("Updated list without duplicates: ");
		System.out.println(newList);

		// List of Integers - Remove duplicates using collection
		List<Integer> numberslist = Arrays.asList(1, 4, 3, 2, 5, 1, 2, 5, 2);
		Set<Integer> numberset = new HashSet<Integer>();
		for(int num : numberslist) {
			numberset.add(num);
		}
		System.out.println("Updated list without duplicates using Set: ");
		System.out.println(numberset);

		// List of integers - Sort elements
		List<Integer> unsortednumbers = Arrays.asList(1, 4, 3, 2, 5, 1, 2, 5, 2);
		List<Integer> sortedList = unsortednumbers.stream().sorted().collect(Collectors.toList());
		System.out.println("Sorted list is: ");
		System.out.println(sortedList);

		// List of Integers - Combining sorted list using stream
		List<Integer> num = Arrays.asList(1, 2, 3, 2, 2, 1, 3, 1, 1);
		List<Integer> num1 = num.stream().filter(x -> x.equals(1)).collect(Collectors.toList());
		List<Integer> num2 = num.stream().filter(x -> x.equals(2)).collect(Collectors.toList());
		List<Integer> num3 = num.stream().filter(x -> x.equals(3)).collect(Collectors.toList());
		List<Integer> numberss = Stream.concat(num1.stream(), num3.stream()).collect(Collectors.toList());
		numberss = Stream.concat(numberss.stream(), num2.stream()).collect(Collectors.toList());
		System.out.println("Custom Arranged List using stream:");
		System.out.println(numberss);

		// List of Integers - Combining sorted list using addAll
		List<Integer> rearrangednums = new ArrayList<>();
		rearrangednums.addAll(num1);
		rearrangednums.addAll(num3);
		rearrangednums.addAll(num2);
		System.out.println("Custom Arranged List using addAll:");
		System.out.println(rearrangednums);
		
		//reverse string without reversing words
		String str = "How are you";
		String[] strArray = str.split(" ");
		String reverse = "";
		for(int i= strArray.length-1; i>=0; i--) {
			reverse = reverse + strArray[i] + " ";
		}
		System.out.println("Reverse string is: ");
		System.out.println(reverse);
		
		//Remove duplicate from Array
		Integer[] arr = {1,2,3,2,1,3,2};
		Set<Integer> st = new HashSet<Integer>();
		for(int i=0; i<arr.length; i++) {
			st.add(arr[i]);
		}
		System.out.println("Remove duplicate using set is: ");
		System.out.println(st);
		
		try {
			return;
		}finally {
			System.out.println("Finally");
		}
	}
}
