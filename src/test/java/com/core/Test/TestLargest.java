package com.core.Test;

import java.util.Arrays;
import java.util.HashMap;

public class TestLargest {
	public static void main(String args[]) {
		largestAndSmallest(new int[] { -20, 34, 21, -87, 92, Integer.MAX_VALUE });
		largestAndSmallest(new int[] { 10, Integer.MIN_VALUE, -2 });
		largestAndSmallest(new int[] { Integer.MAX_VALUE, 40, Integer.MAX_VALUE });
		largestAndSmallest(new int[] { 1, -1, 0 });
		getCount();

		int arr[] = { 5, 1, 2, 6, 4, 4, 5, 6, 8, 7 };
        removeDuplicateUsingSorting(arr);
	}

	public static void largestAndSmallest(int[] numbers) {
		int largest = Integer.MIN_VALUE;
		int smallest = Integer.MAX_VALUE;
		for (int number : numbers) {
			if (number > largest) {
				largest = number;
			} else if (number < smallest) {
				smallest = number;
			}
		}
		System.out.println("Given integer array : " + Arrays.toString(numbers));
		System.out.println("Largest number in array is : " + largest);
		System.out.println("Smallest number in array is : " + smallest);
	}

	public static void getCount() {
		String str = "This this is is done by Saket Saket";
		String[] split = str.split(" ");
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < split.length; i++) {
			if (map.containsKey(split[i])) {
				int count = map.get(split[i]);
				map.put(split[i], count + 1);
			} else {
				map.put(split[i], 1);
			}
		}
		System.out.println(map);
	}

	public static void removeDuplicateUsingSorting(int arr[]) {

		// Sort an unsorted array
		Arrays.sort(arr);

		int len = arr.length;
		int j = 0;

		// Traverse an array
		for (int i = 0; i < len - 1; i++) {

			// if value present at i and i+1 index is not equal
			if (arr[i] != arr[i + 1]) {
				arr[j++] = arr[i];
			}
		}

		arr[j++] = arr[len - 1];

		for (int k = 0; k < j; k++) {
			System.out.print(arr[k] + " ");
		}
	}
}
