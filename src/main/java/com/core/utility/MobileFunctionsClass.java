package com.core.utility;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.core.base.BaseSetup;
import com.core.base.Constants;
import com.core.utility.FunctionsClass.locatorType;
import com.google.common.collect.ImmutableMap;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidStartScreenRecordingOptions;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class MobileFunctionsClass {

	private static final String CLASS_NAME = "classname";
	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String XPATH = "xpath";
	private static final String LINK_TEXT = "linktext";
	private static final String CSS_SELECTOR = "cssselector";
	private static final String TAG_NAME = "tagname";
	private static final String TEXT_IS_NOT_CLEARED = "Text is not cleared";
	AndroidDriver<MobileElement> androidDriver;

	public MobileFunctionsClass(AndroidDriver<MobileElement> androidDriver) {
		this.androidDriver = androidDriver;
	}

	public MobileFunctionsClass() {
		// Blank Constructor
	}

	/** Function to create mobile element using selector */
	public MobileElement createElement(By locator) {
		MobileElement element = null;
		try {
			if (locator != null) {
				element = androidDriver.findElement(locator);
			}
		} catch (Exception e) {
			BaseSetup.logger.error("Locator Not Found" + e);
		}
		return element;
	}

	/** Function to create mobile element using different locators */
	public MobileElement createElement(String locator, locatorType selector) {
		MobileElement element = null;
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					element = androidDriver.findElementByClassName(locator);
					break;
				case ID:
					element = androidDriver.findElementById(locator);
					break;
				case NAME:
					element = androidDriver.findElementByName(locator);
					break;
				case XPATH:
					element = androidDriver.findElementByXPath(locator);
					break;
				case LINK_TEXT:
					element = androidDriver.findElementByLinkText(locator);
					break;
				case CSS_SELECTOR:
					element = androidDriver.findElementByCssSelector(locator);
					break;
				case TAG_NAME:
					element = androidDriver.findElementByTagName(locator);
					break;
				default:
					element = androidDriver.findElementByXPath(locator);
					break;
				}
			} else {
				BaseSetup.logger.error("Locator is null");
			}
		} catch (Exception e) {
			BaseSetup.logger.error("Locator is not correct" + e);
		}
		return element;
	}

	/** Method used to wait until element is found */
	public void waitUntilFound(String locator, locatorType selector, int time) {
		WebDriverWait wait = new WebDriverWait(androidDriver, time);
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
					break;
				case ID:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
					break;
				case NAME:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
					break;
				case XPATH:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
					break;
				case LINK_TEXT:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(locator)));
					break;
				case CSS_SELECTOR:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)));
					break;
				case TAG_NAME:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(locator)));
					break;
				default:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
					break;
				}

			} else {
				BaseSetup.logger.info("Element not found");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to wait until element is clickable */
	public void waitUntilClickable(String locator, locatorType selector, int time) {
		WebDriverWait wait = new WebDriverWait(androidDriver, time);
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					wait.until(ExpectedConditions.elementToBeClickable(By.className(locator)));
					break;
				case ID:
					wait.until(ExpectedConditions.elementToBeClickable(By.id(locator)));
					break;
				case NAME:
					wait.until(ExpectedConditions.elementToBeClickable(By.name(locator)));
					break;
				case XPATH:
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
					break;
				case LINK_TEXT:
					wait.until(ExpectedConditions.elementToBeClickable(By.linkText(locator)));
					break;
				case CSS_SELECTOR:
					wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(locator)));
					break;
				case TAG_NAME:
					wait.until(ExpectedConditions.elementToBeClickable(By.tagName(locator)));
					break;
				default:
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
					break;
				}

			} else {
				BaseSetup.logger.info("Element not clickable");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to click on element using any locator */
	public void clickElement(String locator, locatorType selector) {
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					androidDriver.findElementByClassName(locator).click();
					break;
				case ID:
					androidDriver.findElementById(locator).click();
					break;
				case NAME:
					androidDriver.findElementByName(locator).click();
					break;
				case XPATH:
					androidDriver.findElementByXPath(locator).click();
					break;
				case LINK_TEXT:
					androidDriver.findElementByLinkText(locator).click();
					break;
				case CSS_SELECTOR:
					androidDriver.findElementByCssSelector(locator).click();
					break;
				case TAG_NAME:
					androidDriver.findElementByTagName(locator).click();
					break;
				default:
					androidDriver.findElementByXPath(locator).click();
					break;
				}
			} else {
				BaseSetup.logger.info("Element is null");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to click on element using mobile element */
	public void clickElement(MobileElement element) {
		try {
			if (element != null) {
				element.click();
			} else {
				BaseSetup.logger.info("Elemnt not clickable");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to create list of elements */
	public List<MobileElement> createElements(String locator, locatorType selector) {
		List<MobileElement> elements = null;
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					elements = androidDriver.findElementsByClassName(locator);
					break;
				case ID:
					elements = androidDriver.findElementsById(locator);
					break;
				case NAME:
					elements = androidDriver.findElementsByName(locator);
					break;
				case XPATH:
					elements = androidDriver.findElementsByXPath(locator);
					break;
				case LINK_TEXT:
					elements = androidDriver.findElementsByLinkText(locator);
					break;
				case CSS_SELECTOR:
					elements = androidDriver.findElementsByCssSelector(locator);
					break;
				case TAG_NAME:
					elements = androidDriver.findElementsByTagName(locator);
					break;
				default:
					elements = androidDriver.findElementsByXPath(locator);
					break;
				}
			} else {
				BaseSetup.logger.info("locator not found");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return elements;
	}

	/** Method used to send keys to element */
	public void sendText(WebElement element, String text) {
		try {
			sleep(1);
			if (element != null && text != null) {
				element.sendKeys(text);
			} else {
				BaseSetup.logger.info("Set Text failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to send keys to element using any locator */
	@SuppressWarnings("deprecation")
	public void sendText(String locator, String text, locatorType selector) {
		try {
			sleep(1);
			if (locator != null && text != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					androidDriver.findElementByClassName(locator).click();
					break;
				case ID:
					androidDriver.findElementById(locator).click();
					break;
				case NAME:
					androidDriver.findElementByName(locator).click();
					break;
				case XPATH:
					androidDriver.findElementByXPath(locator).click();
					break;
				case LINK_TEXT:
					androidDriver.findElementByLinkText(locator).click();
					break;
				case CSS_SELECTOR:
					androidDriver.findElementByCssSelector(locator).click();
					break;
				case TAG_NAME:
					androidDriver.findElementByTagName(locator).click();
					break;
				default:
					androidDriver.findElementByXPath(locator).click();
					break;
				}
				androidDriver.getKeyboard().sendKeys(text);
			} else {
				BaseSetup.logger.info("Set Text failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to sleep for a given time in sec */
	public void sleep(int time) {
		try {
			TimeUnit.SECONDS.sleep(time);
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to check if element is visible using xpath */
	public boolean elementVisible(String locator, locatorType selector) {
		boolean result = false;
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					result = androidDriver.findElementByClassName(locator).isDisplayed();
					break;
				case ID:
					result = androidDriver.findElementById(locator).isDisplayed();
					break;
				case NAME:
					result = androidDriver.findElementByName(locator).isDisplayed();
					break;
				case XPATH:
					result = androidDriver.findElementByXPath(locator).isDisplayed();
					break;
				case LINK_TEXT:
					result = androidDriver.findElementByLinkText(locator).isDisplayed();
					break;
				case CSS_SELECTOR:
					result = androidDriver.findElementByCssSelector(locator).isDisplayed();
					break;
				case TAG_NAME:
					result = androidDriver.findElementByTagName(locator).isDisplayed();
					break;
				default:
					result = androidDriver.findElementByXPath(locator).isDisplayed();
					break;
				}
			} else {
				BaseSetup.logger.info("Element not visible");
			}
		} catch (Exception e) {
			result = false;
		}
		return result;
	}

	/** Method used to check if element is visible using element */
	public boolean elementVisible(WebElement element) {
		boolean result = false;
		try {
			if (element != null) {
				result = element.isDisplayed();
			} else {
				BaseSetup.logger.info("Element not visible");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return result;
	}

	/** This function is used to clear the text from the textbox */
	public void clearText(String locator, locatorType selector) {
		MobileElement toClear;
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					toClear = androidDriver.findElementByClassName(locator);
					break;
				case ID:
					toClear = androidDriver.findElementById(locator);
					break;
				case NAME:
					toClear = androidDriver.findElementByName(locator);
					break;
				case XPATH:
					toClear = androidDriver.findElementByXPath(locator);
					break;
				case LINK_TEXT:
					toClear = androidDriver.findElementByLinkText(locator);
					break;
				case CSS_SELECTOR:
					toClear = androidDriver.findElementByCssSelector(locator);
					break;
				case TAG_NAME:
					toClear = androidDriver.findElementByTagName(locator);
					break;
				default:
					toClear = androidDriver.findElementByXPath(locator);
					break;
				}
				String str = toClear.getText();
				toClear.click();
				sleep(1);
				for (int i = 0; i < str.length(); i++) {
					androidDriver.pressKey(new KeyEvent(AndroidKey.DEL));
				}
			} else {
				BaseSetup.logger.info(TEXT_IS_NOT_CLEARED);
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to clear the text from the textbox */
	public void clearText(WebElement element) {
		try {
			if (element != null) {
				sleep(2);
				element.sendKeys(Keys.CONTROL + "a");
				element.sendKeys(Keys.DELETE);
			} else {
				BaseSetup.logger.info(TEXT_IS_NOT_CLEARED);
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to switch to frame using webelement */
	public void switchFrame(WebElement element) {
		try {
			if (element != null) {
				androidDriver.switchTo().frame(element);
			} else {
				BaseSetup.logger.info("Switch to failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to switch to frame using name */
	public void switchFrame(String name) {
		try {
			if (name != null) {
				androidDriver.switchTo().frame(name);
			} else {
				BaseSetup.logger.info("Switch to Failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to switch to default frame */
	public void switchDefaultFrame() {
		try {
			androidDriver.switchTo().defaultContent();
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to get the Text of Element */
	public String getText(String locator, locatorType selector) {
		String txt = null;
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					txt = androidDriver.findElementByClassName(locator).getText();
					break;
				case ID:
					txt = androidDriver.findElementById(locator).getText();
					break;
				case NAME:
					txt = androidDriver.findElementByName(locator).getText();
					break;
				case XPATH:
					txt = androidDriver.findElementByXPath(locator).getText();
					break;
				case LINK_TEXT:
					txt = androidDriver.findElementByLinkText(locator).getText();
					break;
				case CSS_SELECTOR:
					txt = androidDriver.findElementByCssSelector(locator).getText();
					break;
				case TAG_NAME:
					txt = androidDriver.findElementByTagName(locator).getText();
					break;
				default:
					txt = androidDriver.findElementByXPath(locator).getText();
					break;
				}
			} else {
				BaseSetup.logger.info("Text is not found");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return txt;
	}

	/** Method to log message in report with screenshot */
	public void passTest(String message, String tcName) {
		try {
			BaseSetup.test.get().pass(message);
			BaseSetup.test.get().addScreenCaptureFromPath(passScreenCapture(tcName));
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method to log message in report with screenshot */
	public void failTest(String message, String tcName) {
		try {
			BaseSetup.test.get().fail(message);
			BaseSetup.test.get().addScreenCaptureFromPath(failScreenCapture(tcName));
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to capture pass screenshots */
	public String passScreenCapture(String tcName) throws IOException {
		String dest = null;
		try {
			if (tcName != null) {
				Date d = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
				File scrFile = (androidDriver).getScreenshotAs(OutputType.FILE);
				dest = Constants.PASS_TEST_CASE_FOLDER + sdf.format(d) + tcName + Constants.PASS_TEST_CASE_EXTENSION;
				FileUtils.copyFile(scrFile, new File(dest));
				dest = dest.substring(1);
				Path path = FileSystems.getDefault().getPath("").toAbsolutePath();
				dest = path + dest;
			} else {
				BaseSetup.logger.info("Screen Capture Failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return dest;
	}

	/** This function is used to capture failed screenshots */
	public String failScreenCapture(String tcName) throws IOException {
		String dest = null;
		try {
			if (tcName != null) {
				Date d = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HHmmss");
				File scrFile = (androidDriver).getScreenshotAs(OutputType.FILE);
				dest = Constants.FAILED_TEST_CASE_FOLDER + sdf.format(d) + tcName
						+ Constants.FAILED_TEST_CASE_EXTENSION;
				FileUtils.copyFile(scrFile, new File(dest));
				dest = dest.substring(1);
				Path path = FileSystems.getDefault().getPath("").toAbsolutePath();
				dest = path + dest;
			} else {
				BaseSetup.logger.info("Screen Capture Failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return dest;
	}

	@SuppressWarnings("rawtypes")
	public void swipeScreen(Direction dir) {
		BaseSetup.logger.info("swipeScreen(): dir: '" + dir + "'"); // always log your actions

		final int ANIMATION_TIME = 200; // ms

		final int PRESS_TIME = 200; // ms

		int edgeBorder = 10; // better avoid edges
		PointOption pointOptionStart;
		PointOption pointOptionEnd;

		// init screen variables
		Dimension dims = androidDriver.manage().window().getSize();

		// init start point = center of screen
		pointOptionStart = PointOption.point(dims.width / 2, dims.height / 2);

		switch (dir) {
		case DOWN: // center of footer
			pointOptionEnd = PointOption.point(dims.width / 2, dims.height - edgeBorder);
			break;
		case UP: // center of header
			pointOptionEnd = PointOption.point(dims.width / 2, edgeBorder);
			break;
		case LEFT: // center of left side
			pointOptionEnd = PointOption.point(edgeBorder, dims.height / 2);
			break;
		case RIGHT: // center of right side
			pointOptionEnd = PointOption.point(dims.width - edgeBorder, dims.height / 2);
			break;
		default:
			throw new IllegalArgumentException("swipeScreen(): dir: '" + dir + "' NOT supported");
		}

		// execute swipe using TouchAction
		try {
			new TouchAction(androidDriver).press(pointOptionStart)
					.waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME))).moveTo(pointOptionEnd).release()
					.perform();
		} catch (Exception e) {
			BaseSetup.logger.error("swipeScreen(): TouchAction FAILED\n" + e.getMessage());
			return;
		}

		// always allow swipe action to complete
		try {
			Thread.sleep(ANIMATION_TIME);
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	public enum Direction {
		UP, DOWN, LEFT, RIGHT;
	}

	public void hideKeyboard() {
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	@SuppressWarnings("rawtypes")
	public void swipeScreenSmall(Direction dir) {
		BaseSetup.logger.info("swipeScreenSmall(): dir: '" + dir + "'"); // always log your actions

		final int ANIMATION_TIME = 200; // ms

		final int PRESS_TIME = 200; // ms

		PointOption pointOptionStart;
		PointOption pointOptionEnd;

		// init screen variables
		Dimension dims = androidDriver.manage().window().getSize();

		// init start point = center of screen
		pointOptionStart = PointOption.point(dims.width / 2, dims.height / 2);

		// reduce swipe move into multiplier times comparing to swipeScreen move
		int mult = 2; // multiplier
		switch (dir) {
		case DOWN: // center of footer
			pointOptionEnd = PointOption.point(dims.width / 2, (dims.height / 2) + (dims.height / 2) / mult);
			break;
		case UP: // center of header
			pointOptionEnd = PointOption.point(dims.width / 2, (dims.height / 2) - (dims.height / 2) / mult);
			break;
		case LEFT: // center of left side
			pointOptionEnd = PointOption.point((dims.width / 2) - (dims.width / 2) / mult, dims.height / 2);
			break;
		case RIGHT: // center of right side
			pointOptionEnd = PointOption.point((dims.width / 2) + (dims.width / 2) / mult, dims.height / 2);
			break;
		default:
			throw new IllegalArgumentException("swipeScreenSmall(): dir: '" + dir.toString() + "' NOT supported");
		}

		try {
			new TouchAction(androidDriver).press(pointOptionStart)
					.waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME))).moveTo(pointOptionEnd).release()
					.perform();
		} catch (Exception e) {
			BaseSetup.logger.error("swipeScreenSmall(): TouchAction FAILED\n" + e.getMessage());
			return;
		}

		// always allow swipe action to complete
		try {
			Thread.sleep(ANIMATION_TIME);
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method to click Enter */
	public void pressEnter() {
		try {
			androidDriver.pressKey(new KeyEvent(AndroidKey.ENTER));
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method to upload file */
	public boolean keyboardShown() {
		boolean isKeyboardShown = false;
		try {
			isKeyboardShown = androidDriver.isKeyboardShown();
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return isKeyboardShown;
	}

	/** Method to start recording a video */
	public void recordVideo() {
		try {
			(androidDriver).startRecordingScreen(new AndroidStartScreenRecordingOptions().withVideoSize("1280x720"));
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method to stop recording a video */
	public void stopVideo(String testName) {
		try {
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HHmmss");
			String base64String = (androidDriver).stopRecordingScreen();
			byte[] data = Base64.getDecoder().decode(base64String);
			String destinationPath = Constants.VIDEO_FOLDER + sdf.format(d) + testName + ".mp4";
			Path path = Paths.get(destinationPath);
			Files.write(path, data);
			String dest = path.toString().substring(1);
			Path path1 = FileSystems.getDefault().getPath("").toAbsolutePath();
			dest = path1 + dest;
			BaseSetup.test.get().info("Code Coverage Link : <a href='file://" + dest + "'>Recorded Video Link</a>");
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Scroll using JS Executor */
	public void executeScrollUP() {
		try {
			androidDriver.executeScript("mobile: scroll", ImmutableMap.of("direction", "up"));
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Scroll using JS Executor */
	public void getDeviceInfo() {
		try {
			BaseSetup.logger.info("*********************************************");
			BaseSetup.logger.info("Device Info is: " + androidDriver.executeScript("mobile:deviceInfo"));
			BaseSetup.logger.info("*********************************************");
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** FInding element width and height */
	public void elementSize(MobileElement element) {
		try {
			Dimension elementSize = element.getSize();
			BaseSetup.logger.info("Size is: " + elementSize);
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** FInding element location */
	public void elementLocation(MobileElement element) {
		try {
			Point location = element.getLocation();
			BaseSetup.logger.info("Location is: " + location);
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	@SuppressWarnings("rawtypes")
	/** FInding element width and height and clicking on bottom */
	public void clickElementPoint(MobileElement element) {
		try {
			Dimension elementSize = element.getSize();
			int height = elementSize.getHeight();
			int width = elementSize.getWidth();
			Point location = element.getLocation();
			int x = location.getX();
			int y = location.getY();
			int xPoint = ((x + width) - (width / 2));
			int yPoint = ((y + height) - (height / 2));
			TouchAction touchAction = new TouchAction(androidDriver);
			touchAction.tap(PointOption.point(xPoint, yPoint)).perform();
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Find and Click element by Image */
	public void clickElementImage(String imgName) {
		try {
			File file = new File("./src/main/resources/ElementImages/" + imgName);
			File refImgFile = Paths.get(file.toURI()).toFile();
			String s = Base64.getEncoder().encodeToString(Files.readAllBytes(refImgFile.toPath()));
			androidDriver.findElementByImage(s).click();
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

}
