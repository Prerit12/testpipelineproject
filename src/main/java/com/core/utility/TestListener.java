package com.core.utility;

import org.openqa.selenium.WebDriver;
import org.testng.IClassListener;
import org.testng.ITestClass;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentTest;
import com.core.base.BaseSetup;
import com.core.base.Constants;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class TestListener extends BaseSetup implements ITestListener, IClassListener {
	FunctionsClass fc = new FunctionsClass();
	MobileFunctionsClass mfc;
	ExtentTest parent;

	@Override
	public void onStart(ITestContext iTestContext) {
		logger.info("Test Suite Started");
		//fc.deleteVideos();
		fc.createDirectory();
		extent = ExtentReportManager.getInstance();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void onFinish(ITestContext iTestContext) {
		//fc.copyReportToOld();
		driver = (WebDriver) iTestContext.getAttribute("WebDriver");
		androidDriver = (AndroidDriver) iTestContext.getAttribute("MobileDriver");
		if (service != null) {
			androidDriver.removeApp(Constants.APP_NAME);
			service.stop();
		}
		if (driver != null) {
			driver.quit();
		}
		logger.info("Test Suite Finished");
		extent.flush();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onTestStart(ITestResult iTestResult) {
		logger.info("Test case Started: " + iTestResult.getMethod().getMethodName());
		ExtentTest child = parentTest.get().createNode(iTestResult.getMethod().getMethodName());
		test.set(child);
		ITestContext context = iTestResult.getTestContext();
		androidDriver = (AndroidDriver<MobileElement>) context.getAttribute("MobileDriver");
		if (androidDriver != null) {
			mfc = new MobileFunctionsClass(androidDriver);
			mfc.recordVideo();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onTestSuccess(ITestResult iTestResult) {
		logger.info("Test case Success: " + iTestResult.getMethod().getMethodName());
		test.get().pass("Test passed");
		ITestContext context = iTestResult.getTestContext();
		androidDriver = (AndroidDriver<MobileElement>) context.getAttribute("MobileDriver");
		if (androidDriver != null) {
			mfc = new MobileFunctionsClass(androidDriver);
			mfc.stopVideo(iTestResult.getMethod().getMethodName());
		}
		extent.flush();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onTestFailure(ITestResult iTestResult) {
		logger.error("Test case Failed: " + iTestResult.getMethod().getMethodName());
		test.get().fail(iTestResult.getThrowable());
		ITestContext context = iTestResult.getTestContext();
		androidDriver = (AndroidDriver<MobileElement>) context.getAttribute("MobileDriver");
		if (androidDriver != null) {
			mfc = new MobileFunctionsClass(androidDriver);
			mfc.stopVideo(iTestResult.getMethod().getMethodName());
		}
		extent.flush();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onTestSkipped(ITestResult iTestResult) {
		logger.warn("Test case Skipped: " + iTestResult.getMethod().getMethodName());
		test.get().skip(iTestResult.getThrowable());
		ITestContext context = iTestResult.getTestContext();
		androidDriver = (AndroidDriver<MobileElement>) context.getAttribute("MobileDriver");
		if (androidDriver != null) {
			mfc = new MobileFunctionsClass(androidDriver);
			mfc.stopVideo(iTestResult.getMethod().getMethodName());
		}
		extent.flush();
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
		// Functionality not required
	}

	@Override
	public void onBeforeClass(ITestClass testClass) {
		logger.info("Before Class Started: " + testClass.getName());
		String className = testClass.getName();
		parent = extent.createTest(className);
		parentTest.set(parent);
	}

	@Override
	public void onAfterClass(ITestClass testClass) {
		// TODO Auto-generated method stub

	}
}
