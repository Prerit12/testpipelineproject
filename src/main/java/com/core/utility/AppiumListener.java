package com.core.utility;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.core.base.BaseSetup;

import io.appium.java_client.events.api.general.AppiumWebDriverEventListener;

public class AppiumListener implements AppiumWebDriverEventListener {

	@Override
	public void beforeAlertAccept(WebDriver driver) {
		BaseSetup.logger.info("Clicking on Alert Accept Button");

	}

	@Override
	public void afterAlertAccept(WebDriver driver) {
		BaseSetup.logger.info("Clicked on Alert Accept Button");

	}

	@Override
	public void afterAlertDismiss(WebDriver driver) {
		BaseSetup.logger.info("Clicking on Alert Dismiss Button");

	}

	@Override
	public void beforeAlertDismiss(WebDriver driver) {
		BaseSetup.logger.info("Clicked on Alert Dismiss Button");

	}

	@Override
	public void beforeNavigateTo(String url, WebDriver driver) {
		BaseSetup.logger.info("Navigating to URL: " + url);

	}

	@Override
	public void afterNavigateTo(String url, WebDriver driver) {
		BaseSetup.logger.info("Navigated to URL: " + url);

	}

	@Override
	public void beforeNavigateBack(WebDriver driver) {
		BaseSetup.logger.info("Navigating Back to URL");

	}

	@Override
	public void afterNavigateBack(WebDriver driver) {
		BaseSetup.logger.info("Navigated Back to URL");

	}

	@Override
	public void beforeNavigateForward(WebDriver driver) {
		BaseSetup.logger.info("Navigating Forward to URL");

	}

	@Override
	public void afterNavigateForward(WebDriver driver) {
		BaseSetup.logger.info("Navigated Forward to URL");

	}

	@Override
	public void beforeNavigateRefresh(WebDriver driver) {
		BaseSetup.logger.info("Refreshing the page");

	}

	@Override
	public void afterNavigateRefresh(WebDriver driver) {
		BaseSetup.logger.info("Page is refreshed");

	}

	@Override
	public void beforeFindBy(By by, WebElement element, WebDriver driver) {
		BaseSetup.logger.info("Finding element by: " + by + "and element: " + element);

	}

	@Override
	public void afterFindBy(By by, WebElement element, WebDriver driver) {
		BaseSetup.logger.info("After Finding element by: " + by + "and element: " + element);

	}

	@Override
	public void beforeClickOn(WebElement element, WebDriver driver) {
		BaseSetup.logger.info("Clicking on element: " + element);

	}

	@Override
	public void afterClickOn(WebElement element, WebDriver driver) {
		BaseSetup.logger.info("Clicked on element: " + element);

	}

	@Override
	public void beforeChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
		BaseSetup.logger.info("Changing element: " + element + "value: " + keysToSend);

	}

	@Override
	public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
		BaseSetup.logger.info("Changed element: " + element + "value: " + keysToSend);

	}

	@Override
	public void beforeScript(String script, WebDriver driver) {
		BaseSetup.logger.info("Before running Script: " + script);

	}

	@Override
	public void afterScript(String script, WebDriver driver) {
		BaseSetup.logger.info("After running Script: " + script);

	}

	@Override
	public void beforeSwitchToWindow(String windowName, WebDriver driver) {
		BaseSetup.logger.info("Before switching Window: " + windowName);

	}

	@Override
	public void afterSwitchToWindow(String windowName, WebDriver driver) {
		BaseSetup.logger.info("After switching Window: " + windowName);

	}

	@Override
	public void onException(Throwable throwable, WebDriver driver) {
		BaseSetup.logger.info("Exception occured: " + throwable);

	}

	@Override
	public <X> void beforeGetScreenshotAs(OutputType<X> target) {
		BaseSetup.logger.info("Taking Screenshot");

	}

	@Override
	public <X> void afterGetScreenshotAs(OutputType<X> target, X screenshot) {
		BaseSetup.logger.info("Screenshot Taken");

	}

	@Override
	public void beforeGetText(WebElement element, WebDriver driver) {
		BaseSetup.logger.info("Getting Text of element: " + element);

	}

	@Override
	public void afterGetText(WebElement element, WebDriver driver, String text) {
		BaseSetup.logger.info("After Getting Text of element: " + element + "and Text: " + text);

	}

	@Override
	public void afterChangeValueOf(WebElement arg0, WebDriver arg1) {
		BaseSetup.logger.info("After change value of element: " + arg0);

	}

	@Override
	public void beforeChangeValueOf(WebElement arg0, WebDriver arg1) {
		BaseSetup.logger.info("Before change value of element: " + arg0);

	}

}
