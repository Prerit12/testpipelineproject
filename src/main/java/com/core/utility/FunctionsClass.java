package com.core.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessBuffer;
import org.apache.pdfbox.io.RandomAccessRead;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.Logs;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.core.base.Constants;
import com.core.base.BaseSetup;

public class FunctionsClass {
	private static final String TAG_NAME = "tagname";
	private static final String CSS_SELECTOR = "cssselector";
	private static final String LINK_TEXT = "linktext";
	private static final String XPATH = "xpath";
	private static final String NAME = "name";
	private static final String ID = "id";
	private static final String CLASS_NAME = "classname";
	private static final String TEXT_IS_NOT_CLEARED = "Text is not cleared";
	WebDriver driver;
	private static String directory = "./Current_test_results/Reports/";
	private static String directoryVideo = "./Current_test_results/Reports/video/";
	private static String oldReportsDirectory = "./Arhived_test_results/";
	private static String subDirectory = "screenshots/";

	public FunctionsClass(WebDriver driver) {
		this.driver = driver;
	}

	public FunctionsClass() {
		// Blank Constructor
	}

	private static FunctionsClass single_instance = null;

	public static FunctionsClass Instance() {
		if (single_instance == null) {
			single_instance = new FunctionsClass();
		}
		return single_instance;
	}

	public static FunctionsClass Instance(WebDriver driver) {
		if (single_instance == null) {
			single_instance = new FunctionsClass(driver);
		}
		return single_instance;
	}

	/**
	 * Function to read Excel Data and getting the result in List for a particular
	 * row
	 */
	public List<String> ExcelReader(String sheetName, String columnName) throws IOException {
		List<String> excelData = new ArrayList<>();
		Workbook wb = null;
		try {
			FileInputStream fis = new FileInputStream("./src/main/Resources/TestData.xlsx");
			wb = WorkbookFactory.create(fis);
			Sheet sheet = wb.getSheet(sheetName);
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Iterator<Cell> cellIterator = row.cellIterator();

				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					if (cell.toString().equalsIgnoreCase(columnName)) {
						int rowNum = cell.getRowIndex();
						for (int i = rowNum; i <= rowNum; i++) {
							for (int j = 1; j < sheet.getRow(rowNum).getLastCellNum(); j++) {
								Row row1 = sheet.getRow(i);
								if ((row1.getCell(j).getCellType().toString()).contains("NUMERIC")) {
									Cell cell1 = row1.getCell(j);
									excelData.add(NumberToTextConverter.toText(cell1.getNumericCellValue()));
								} else {
									Cell cell1 = row1.getCell(j);
									excelData.add(cell1.toString());
								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			BaseSetup.logger.error("Excel File is not readable" + e);
		} finally {
			if (wb != null) {
				wb.close();
			}
		}
		return excelData;
	}

	public List<String> ExcelReaderColumn(String sheetName, String columnName) throws IOException {
		List<String> excelData = new ArrayList<>();
		Workbook wb = null;
		try {
			FileInputStream fis = new FileInputStream("./src/main/Resources/TestData.xlsx");
			wb = WorkbookFactory.create(fis);
			Sheet sheet = wb.getSheet(sheetName);
			Row row = sheet.getRow(0);

			Iterator<Cell> cellIterator = row.cellIterator();

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				if (cell.toString().equalsIgnoreCase(columnName)) {
					int columnNum = cell.getColumnIndex();
					for (int i = columnNum; i <= columnNum; i++) {
						for (int j = 1; j <= sheet.getLastRowNum(); j++) {
							if (sheet.getRow(j) != null) {
								Row row1 = sheet.getRow(j);
								if ((row1.getCell(i).getCellType().toString()).isEmpty()
										|| (row1.getCell(i).getCellType().toString()).contains("BLANK")) {
									break;
								} else if ((row1.getCell(i).getCellType().toString()).contains("NUMERIC")) {
									Cell cell1 = row1.getCell(i);
									excelData.add(NumberToTextConverter.toText(cell1.getNumericCellValue()));
								} else {
									Cell cell1 = row1.getCell(i);
									excelData.add(cell1.toString());
								}
							} else {
								break;
							}
						}
					}
				}
			}

		} catch (Exception e) {
			BaseSetup.logger.error("Excel File is not readable" + e);
		} finally {
			if (wb != null) {
				wb.close();
			}
		}
		return excelData;
	}

	/** Function to create web element using selector */
	public WebElement createElement(By locator) {
		WebElement element = null;
		try {
			if (locator != null) {
				element = driver.findElement(locator);
			}
		} catch (Exception e) {
			BaseSetup.logger.error("Locator Not Found" + e);
		}
		return element;
	}

	/** Function to create web element using xpath */
	public WebElement createElement(String xpath) {
		WebElement element = null;
		try {
			if (xpath != null) {
				element = driver.findElement(By.xpath(xpath));
			}
		} catch (Exception e) {
			BaseSetup.logger.error("XPath is not correct" + e);
		}
		return element;
	}

	/** Function to create mobile element using different locators */
	public WebElement createElement(String locator, locatorType selector) {
		WebElement element = null;
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					element = driver.findElement(By.className(locator));
					break;
				case ID:
					element = driver.findElement(By.id(locator));
					break;
				case NAME:
					element = driver.findElement(By.name(locator));
					break;
				case XPATH:
					element = driver.findElement(By.xpath(locator));
					break;
				case LINK_TEXT:
					element = driver.findElement(By.linkText(locator));
					break;
				case CSS_SELECTOR:
					element = driver.findElement(By.cssSelector(locator));
					break;
				case TAG_NAME:
					element = driver.findElement(By.tagName(locator));
					break;
				default:
					element = driver.findElement(By.xpath(locator));
					break;
				}
			} else {
				BaseSetup.logger.error("Locator is null");
			}
		} catch (Exception e) {
			BaseSetup.logger.error("Locator is not correct" + e);
		}
		return element;
	}

	/** Enum for Locators */
	public enum locatorType {
		XPATH, NAME, ID, CLASSNAME, LINKTEXT, CSSSELECTOR, TAGNAME,
	}

	/** Method used to wait until element is found */
	public void waitUntilFound(String locator, locatorType selector, int time) {
		WebDriverWait wait = new WebDriverWait(driver, time);
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
					break;
				case ID:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
					break;
				case NAME:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
					break;
				case XPATH:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
					break;
				case LINK_TEXT:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(locator)));
					break;
				case CSS_SELECTOR:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)));
					break;
				case TAG_NAME:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(locator)));
					break;
				default:
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
					break;
				}
			} else {
				BaseSetup.logger.info("Element not found");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to wait until element is Clickable */
	public void waitUntilClickable(String locator, locatorType selector, int time) {
		WebDriverWait wait = new WebDriverWait(driver, time);
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					wait.until(ExpectedConditions.elementToBeClickable(By.className(locator)));
					break;
				case ID:
					wait.until(ExpectedConditions.elementToBeClickable(By.id(locator)));
					break;
				case NAME:
					wait.until(ExpectedConditions.elementToBeClickable(By.name(locator)));
					break;
				case XPATH:
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
					break;
				case LINK_TEXT:
					wait.until(ExpectedConditions.elementToBeClickable(By.linkText(locator)));
					break;
				case CSS_SELECTOR:
					wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(locator)));
					break;
				case TAG_NAME:
					wait.until(ExpectedConditions.elementToBeClickable(By.tagName(locator)));
					break;
				default:
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
					break;
				}

			} else {
				BaseSetup.logger.info("Element not clickable");
			}

		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to click on element using xpath */
	public void clickElement(String locator, locatorType selector, int time) {
		WebDriverWait wait = new WebDriverWait(driver, time);
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					wait.until(ExpectedConditions.elementToBeClickable(By.className(locator)));
					driver.findElement(By.className(locator)).click();
					break;
				case ID:
					wait.until(ExpectedConditions.elementToBeClickable(By.id(locator)));
					driver.findElement(By.id(locator)).click();
					break;
				case NAME:
					wait.until(ExpectedConditions.elementToBeClickable(By.name(locator)));
					driver.findElement(By.name(locator)).click();
					break;
				case XPATH:
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
					driver.findElement(By.xpath(locator)).click();
					break;
				case LINK_TEXT:
					wait.until(ExpectedConditions.elementToBeClickable(By.linkText(locator)));
					driver.findElement(By.linkText(locator)).click();
					break;
				case CSS_SELECTOR:
					wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(locator)));
					driver.findElement(By.cssSelector(locator)).click();
					break;
				case TAG_NAME:
					wait.until(ExpectedConditions.elementToBeClickable(By.tagName(locator)));
					driver.findElement(By.tagName(locator)).click();
					break;
				default:
					driver.findElement(By.xpath(locator)).click();
					break;
				}
			} else {
				BaseSetup.logger.info("Element is null");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to click on element using web element */
	public void clickElement(WebElement element) {
		try {
			if (element != null) {
				element.click();
			} else {
				BaseSetup.logger.info("Elemnt not clickable");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to create list of elements */
	public List<WebElement> createElements(String locator, locatorType selector) {
		List<WebElement> elements = null;
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					elements = driver.findElements(By.className(locator));
					break;
				case ID:
					elements = driver.findElements(By.id(locator));
					break;
				case NAME:
					elements = driver.findElements(By.name(locator));
					break;
				case XPATH:
					elements = driver.findElements(By.xpath(locator));
					break;
				case LINK_TEXT:
					elements = driver.findElements(By.linkText(locator));
					break;
				case CSS_SELECTOR:
					elements = driver.findElements(By.cssSelector(locator));
					break;
				case TAG_NAME:
					elements = driver.findElements(By.tagName(locator));
					break;
				default:
					elements = driver.findElements(By.xpath(locator));
					break;
				}
			} else {
				BaseSetup.logger.info("locator not found");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return elements;
	}

	/** Method used to send keys to element */
	public void sendText(WebElement element, String text) {
		try {
			sleep(1);
			if (element != null && text != null) {
				element.sendKeys(text);
			} else {
				BaseSetup.logger.info("Set Text failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to send keys to element using xpath */
	public void sendText(String locator, String text, locatorType selector) {
		try {
			sleep(1);
			if (locator != null && text != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					driver.findElement(By.className(locator)).sendKeys(text);
					break;
				case ID:
					driver.findElement(By.id(locator)).sendKeys(text);
					break;
				case NAME:
					driver.findElement(By.name(locator)).sendKeys(text);
					break;
				case XPATH:
					driver.findElement(By.xpath(locator)).sendKeys(text);
					break;
				case LINK_TEXT:
					driver.findElement(By.linkText(locator)).sendKeys(text);
					break;
				case CSS_SELECTOR:
					driver.findElement(By.cssSelector(locator)).sendKeys(text);
					break;
				case TAG_NAME:
					driver.findElement(By.tagName(locator)).sendKeys(text);
					break;
				default:
					driver.findElement(By.xpath(locator)).sendKeys(text);
					break;
				}
			} else {
				BaseSetup.logger.info("Set Text failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to check if element is visible using xpath */
	public boolean elementVisible(String locator, locatorType selector) {
		boolean result = false;
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					result = driver.findElement(By.className(locator)).isDisplayed();
					break;
				case ID:
					result = driver.findElement(By.id(locator)).isDisplayed();
					break;
				case NAME:
					result = driver.findElement(By.name(locator)).isDisplayed();
					break;
				case XPATH:
					result = driver.findElement(By.xpath(locator)).isDisplayed();
					break;
				case LINK_TEXT:
					result = driver.findElement(By.linkText(locator)).isDisplayed();
					break;
				case CSS_SELECTOR:
					result = driver.findElement(By.cssSelector(locator)).isDisplayed();
					break;
				case TAG_NAME:
					result = driver.findElement(By.tagName(locator)).isDisplayed();
					break;
				default:
					result = driver.findElement(By.xpath(locator)).isDisplayed();
					break;
				}
			} else {
				BaseSetup.logger.info("Element not visible");
			}
		} catch (Exception e) {
			result = false;
		}
		return result;
	}

	/** Method used to check if element is visible using element */
	public boolean elementVisible(WebElement element) {
		boolean result = false;
		try {
			if (element != null) {
				result = element.isDisplayed();
			} else {
				BaseSetup.logger.info("Element not visible");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return result;
	}

	/**
	 * Method used to check if text is present in pdf or not
	 * 
	 * @throws IOException
	 */
	public static boolean isPresentPDF(String url, String text) throws IOException {
		boolean flag = false;
		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;

		try {
			URL url1 = new URL(url);
			RandomAccessRead file = new RandomAccessBuffer(url1.openStream());
			PDFParser parser = new PDFParser(file);

			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);

			pdDoc = new PDDocument(cosDoc);
			int count = pdDoc.getNumberOfPages();
			pdfStripper.setEndPage(count);
			parsedText = pdfStripper.getText(pdDoc);
		} catch (MalformedURLException e2) {
			BaseSetup.logger.error(e2);
		} catch (IOException e) {
			BaseSetup.logger.error(e);
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				BaseSetup.logger.error(e);
			}
		}
		if (parsedText != null && parsedText.contains(text)) {
			flag = true;
			pdDoc.close();
			cosDoc.close();
		}
		return flag;
	}

	/** This function is used to clear the text from the textbox */
	public void clearText(String locator, locatorType selector) {
		WebElement toClear;
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					toClear = driver.findElement(By.className(locator));
					break;
				case ID:
					toClear = driver.findElement(By.id(locator));
					break;
				case NAME:
					toClear = driver.findElement(By.name(locator));
					break;
				case XPATH:
					toClear = driver.findElement(By.xpath(locator));
					break;
				case LINK_TEXT:
					toClear = driver.findElement(By.linkText(locator));
					break;
				case CSS_SELECTOR:
					toClear = driver.findElement(By.cssSelector(locator));
					break;
				case TAG_NAME:
					toClear = driver.findElement(By.tagName(locator));
					break;
				default:
					toClear = driver.findElement(By.xpath(locator));
					break;
				}
				String str = toClear.getText();
				sleep(1);
				for (int i = 0; i < str.length(); i++) {
					toClear.sendKeys(Keys.BACK_SPACE);
				}
			} else {
				BaseSetup.logger.info(TEXT_IS_NOT_CLEARED);
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to clear the text from the textbox */
	public void clearText(WebElement element) {
		try {
			if (element != null) {
				sleep(2);
				element.sendKeys(Keys.CONTROL + "a");
				element.sendKeys(Keys.DELETE);
			} else {
				BaseSetup.logger.info(TEXT_IS_NOT_CLEARED);
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to switch to frame using webelement */
	public void switchFrame(WebElement element) {
		try {
			if (element != null) {
				driver.switchTo().frame(element);
			} else {
				BaseSetup.logger.info("Switch to failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to switch to frame using name */
	public void switchFrame(String name) {
		try {
			if (name != null) {
				driver.switchTo().frame(name);
			} else {
				BaseSetup.logger.info("Switch to Failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to switch to default frame */
	public void switchDefaultFrame() {
		try {
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to sleep for a given time in sec */
	public void sleep(int time) {
		try {
			TimeUnit.SECONDS.sleep(time);
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to get the Text of Element */
	public String getText(String locator, locatorType selector) {
		String txt = null;
		try {
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					txt = driver.findElement(By.className(locator)).getText();
					break;
				case ID:
					txt = driver.findElement(By.id(locator)).getText();
					break;
				case NAME:
					txt = driver.findElement(By.name(locator)).getText();
					break;
				case XPATH:
					txt = driver.findElement(By.xpath(locator)).getText();
					break;
				case LINK_TEXT:
					txt = driver.findElement(By.linkText(locator)).getText();
					break;
				case CSS_SELECTOR:
					txt = driver.findElement(By.cssSelector(locator)).getText();
					break;
				case TAG_NAME:
					txt = driver.findElement(By.tagName(locator)).getText();
					break;
				default:
					txt = driver.findElement(By.xpath(locator)).getText();
					break;
				}
			} else {
				BaseSetup.logger.info(TEXT_IS_NOT_CLEARED);
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return txt;
	}

	/** This function is used to get the Attribute of Element */
	public String getAttribute(String xpath, String attName) {
		String txt = null;
		try {
			txt = driver.findElement(By.xpath(xpath)).getAttribute(attName);
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return txt;
	}

	/** This function is used to execute JS script to move to element */
	public void executeJS(String xpath) {
		try {
			if (xpath != null) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView();", createElement(xpath));
			} else {
				BaseSetup.logger.info("Script execution failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** This function is used to capture pass screenshots */
	public String passScreenCapture(String tcName) throws IOException {
		String dest = null;
		try {
			if (tcName != null) {
				Date d = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				dest = Constants.PASS_TEST_CASE_FOLDER + sdf.format(d) + tcName + Constants.PASS_TEST_CASE_EXTENSION;
				FileUtils.copyFile(scrFile, new File(dest));
				System.out.println("DDestination path is: " +dest);
				dest = dest.substring(1);
				Path path = FileSystems.getDefault().getPath("").toAbsolutePath();
				dest = path + dest;
			} else {
				System.out.println("DDestination path is: " +dest);
				BaseSetup.logger.info("Screen Capture Failed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return dest;
	}

	/** This function is used to capture failed screenshots */
	public String failScreenCapture(String tcName) throws IOException {
		String dest = null;
		try {
			if (tcName != null) {
				Date d = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HHmmss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				dest = Constants.FAILED_TEST_CASE_FOLDER + sdf.format(d) + tcName
						+ Constants.FAILED_TEST_CASE_EXTENSION;
				System.out.println("DDestination path is: " +dest);
				FileUtils.copyFile(scrFile, new File(dest));
				dest = dest.substring(1);
				Path path = FileSystems.getDefault().getPath("").toAbsolutePath();
				dest = path + dest;
			} else {
				BaseSetup.logger.info("Screen Capture Failed");
			}
		} catch (Exception e) {
			System.out.println("DDestination path is: " +dest);
			BaseSetup.logger.error(e);
		}
		return dest;
	}

	/** This function is used to establish DB connection */
	public static Connection dbConnection() {
		Connection c = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Spares", "postgres", "root");
		} catch (Exception e) {
			BaseSetup.logger.error(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		BaseSetup.logger.info("Opened database successfully");
		return c;
	}

	/** Method use to set the zoom level of page */
	public void zoomInZoomOut(String value) {
		if (value != null) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("document.body.style.zoom='" + value + "'");
		} else {
			BaseSetup.logger.info("Zoom is not working");
		}
	}

	/** Method use doubleclick the element */
	public void dblClick(String value) {
		if (value != null) {
			Actions actions = new Actions(driver);
			WebElement elementLocator = driver.findElement(By.xpath(value));
			actions.doubleClick(elementLocator).perform();
		} else {
			BaseSetup.logger.info("Double click failed");
		}
	}

	/** Method use to hover the element */
	public void mouseHover(String value) {
		if (value != null) {
			Actions actions = new Actions(driver);
			WebElement elementLocator = driver.findElement(By.xpath(value));
			actions.moveToElement(elementLocator).build().perform();
		} else {
			BaseSetup.logger.info("Mouse hover not working");
		}
	}

	/** Method use to get console error */
	public void verifyConsoleErrors() {
		Logs logs = driver.manage().logs();
		LogEntries logEntries = logs.get(LogType.BROWSER);
		@SuppressWarnings("deprecation")
		List<LogEntry> errorLogs = logEntries.filter(Level.SEVERE);

		if (!errorLogs.isEmpty()) {
			for (LogEntry logEntry : logEntries) {
				BaseSetup.logger.error("Found error in logs: " + logEntry.getMessage());
			}
		}
	}

	/** Method use to create directory */
	public void createDirectory() {
		try {
			File file = new File(directory + subDirectory);
			FileUtils.forceMkdir(file);
			FileUtils.cleanDirectory(file);
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/**
	 * Method to archive reports at the end of the suite execution by calling it via
	 * aftersuite annotation
	 */
	public void copyReportToOld() {
		File oldDirectory = new File(directory);
		DateFormat dateFormat = new SimpleDateFormat("MMM_dd_yyyy_hh_mm_ssaa");
		String dateName = dateFormat.format(new Date());
		File newDirectory = new File(oldReportsDirectory + dateName);
		try {
			FileUtils.copyDirectory(oldDirectory, newDirectory);
		} catch (IOException e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method to log message in report with screenshot */
	public void passTest(String message, String tcName) {
		try {
			BaseSetup.test.get().pass(message);
			BaseSetup.test.get().addScreenCaptureFromPath(passScreenCapture(tcName));
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method to log message in report with screenshot */
	public void failTest(String message, String tcName) {
		try {
			BaseSetup.test.get().fail(message);
			BaseSetup.test.get().addScreenCaptureFromPath(failScreenCapture(tcName));
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method to click Enter */
	public void pressEnter(String locator, locatorType selector) {
		try {
			sleep(1);
			if (locator != null) {
				switch (selector.toString().toLowerCase()) {
				case CLASS_NAME:
					driver.findElement(By.className(locator)).sendKeys(Keys.ENTER);
					break;
				case ID:
					driver.findElement(By.id(locator)).sendKeys(Keys.ENTER);
					break;
				case NAME:
					driver.findElement(By.name(locator)).sendKeys(Keys.ENTER);
					break;
				case XPATH:
					driver.findElement(By.xpath(locator)).sendKeys(Keys.ENTER);
					break;
				case LINK_TEXT:
					driver.findElement(By.linkText(locator)).sendKeys(Keys.ENTER);
					break;
				case CSS_SELECTOR:
					driver.findElement(By.cssSelector(locator)).sendKeys(Keys.ENTER);
					break;
				case TAG_NAME:
					driver.findElement(By.tagName(locator)).sendKeys(Keys.ENTER);
					break;
				default:
					driver.findElement(By.xpath(locator)).sendKeys(Keys.ENTER);
					break;
				}
			} else {
				BaseSetup.logger.info("Enter is not pressed");
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	/** Method used to replace String characters */
	public String stringRep(String query, String replace, String replaceWith) {
		try {
			String replaceString = query.replace(replace, replaceWith);
			return replaceString;
		} catch (Exception e) {
			BaseSetup.logger.error(e);
			return null;
		}
	}

	/** Method used get DB results from query passed */
	public String getDBResult(Connection c, String querySQL, String columnName) throws SQLException {

		Statement stmt = null;
		ResultSet rs = null;
		String output = null;
		try {
			stmt = c.createStatement();
			rs = stmt.executeQuery(querySQL);
			while (rs.next()) {
				output = rs.getString(columnName);
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		} finally {
			if (rs != null) {
				rs.close();
				stmt.close();
			}
			c.close();
		}
		return output;
	}

	public void deleteVideos() {
		try {
			File videoDirectory = new File(directoryVideo);
			File[] files = videoDirectory.listFiles();
			for (File file : files) {
				file.delete();
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
	}

	public String getLocalStorageData() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			Object key = js.executeScript("return localStorage.key(0);");
			Object value = js.executeScript("return window.localStorage.getItem('" + key + "');");
			return value.toString();
		} catch (Exception e) {
			BaseSetup.logger.error(e);
		}
		return null;
	}
}
