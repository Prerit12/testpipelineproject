package com.core.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.core.base.Constants;
import com.core.utility.FunctionsClass;
import com.core.utility.FunctionsClass.locatorType;

public class TestPage {
	WebDriver driver;
	FunctionsClass fc;

	public TestPage(WebDriver driver) {
		this.driver = driver;
		fc = FunctionsClass.Instance(driver);
	}

	private static TestPage single_instance = null;

	public static TestPage Instance(WebDriver driver) {
		if (single_instance == null) {
			single_instance = new TestPage(driver);
		}
		return single_instance;
	}

	public void Login() {
		driver.get("https://test-radiometer-cn.radiometer.cn/");
		driver.manage().window().maximize();

		driver.findElement(By.xpath("(//a[contains(@class,'service-nav__login')])[1]")).click();
		// fc.clickElement(Constants.BTN_LOGIN, locatorType.XPATH);

		fc.waitUntilFound(Constants.BTN_CONTINUE, locatorType.XPATH, 30);
		driver.findElement(By.xpath("//a[contains(@class,'banner-redirectlink')]")).click();
		// fc.clickElement(Constants.BTN_CONTINUE, locatorType.XPATH);

		fc.waitUntilFound(Constants.BTN_REGISTER, locatorType.ID, 30);
		driver.findElement(By.id("loginSubmit")).click();
		// fc.clickElement(Constants.BTN_REGISTER, locatorType.ID);

		fc.waitUntilFound(Constants.TXT_USERNAME_TEST, locatorType.NAME, 30);
		fc.sendText(Constants.TXT_USERNAME_TEST, "test@test.com", locatorType.NAME);

		driver.findElement(By.xpath("//input[@type='submit']")).click();
		// fc.clickElement(Constants.BTN_NEXT, locatorType.XPATH);

		fc.waitUntilFound(Constants.TXT_PASSWORD, locatorType.NAME, 30);
		fc.sendText(Constants.TXT_PASSWORD, "password", locatorType.NAME);
	}

}
