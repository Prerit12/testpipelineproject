package com.core.Pages;

import org.openqa.selenium.WebDriver;

import com.core.base.BaseSetup;
import com.core.base.Constants;
import com.core.utility.FunctionsClass;
import com.core.utility.FunctionsClass.locatorType;

public class GooglePage {
	WebDriver driver;
	FunctionsClass fc;

	public GooglePage(WebDriver driver) {
		this.driver = driver;
		fc = new FunctionsClass(driver);
	}

	private static GooglePage single_instance = null;
	
	public static GooglePage Instance(WebDriver driver)
    {
        if (single_instance == null) {
            single_instance = new GooglePage(driver);
        }
        return single_instance;
    }

	public void searchGoogle() {
		String tcName = new Throwable().getStackTrace()[0].getClassName();
		try {
			fc.sendText(Constants.TXT_SEARCH, "Test Selenium", locatorType.NAME);
			fc.pressEnter(Constants.TXT_SEARCH, locatorType.NAME);
			if (fc.getText(Constants.TXT_SEARCHED_TEXT, locatorType.XPATH).contains("Selenium")) {
				fc.passTest("Searched Text is visible", tcName);
			} else {
				fc.failTest("Searched text is not Visible", tcName);
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
			fc.failTest("Searched text is not Visible", tcName);
		}
	}

	public void launchURL(String url) {
		try {
			driver.get(url);
			driver.manage().window().maximize();
		}
		catch(Exception e) {
			BaseSetup.logger.error(e);
		}
	}
}
