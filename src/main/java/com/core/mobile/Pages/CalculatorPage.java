package com.core.mobile.Pages;

import com.core.base.BaseSetup;
import com.core.base.Constants;
import com.core.utility.MobileFunctionsClass;
import com.core.utility.FunctionsClass.locatorType;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class CalculatorPage{
	AndroidDriver<MobileElement> androidDriver;
	MobileFunctionsClass mfc;
	
	public CalculatorPage(AndroidDriver<MobileElement> androidDriver) {
		this.androidDriver = androidDriver;
		mfc = new MobileFunctionsClass(androidDriver);
	}

	public void sumTwoDigits() {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();
		try {
			mfc.clickElement(Constants.BUTTON_NUM8, locatorType.ID);
			mfc.clickElement(Constants.BUTTON_ADD,locatorType.ID);
			mfc.clickElement(Constants.BUTTON_NUM7,locatorType.ID);
			if(mfc.getText(Constants.BUTTON_SUM,locatorType.ID).contains("15")) {
				mfc.passTest("Addition is working", tcName);
			}else {
				mfc.failTest("Addition is not working", tcName);
			}
		} catch (Exception e) {
			BaseSetup.logger.error(e);
			mfc.failTest("Addition is not working", tcName);
		}

	}
}
