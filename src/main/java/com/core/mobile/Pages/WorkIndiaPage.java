package com.core.mobile.Pages;

import com.core.base.BaseSetup;
import com.core.base.Constants;
import com.core.utility.FunctionsClass.locatorType;
import com.core.utility.MobileFunctionsClass;
import com.core.utility.MobileFunctionsClass.Direction;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class WorkIndiaPage {
	AndroidDriver<MobileElement> androidDriver;
	MobileFunctionsClass mfc;
	
	public WorkIndiaPage(AndroidDriver<MobileElement> androidDriver) {
		this.androidDriver = androidDriver;
		mfc = new MobileFunctionsClass(androidDriver);
	}
	
	public void Calculator() {
		//String tcName = new Throwable().getStackTrace()[0].getMethodName();
		try {
			mfc.clickElementImage("Next.PNG");
		}catch(Exception e) {
			BaseSetup.logger.error(e);
		}
	}
	
	public void login() {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();
		try {
			mfc.sendText(Constants.TXT_USERNAME, "Prerit", locatorType.ID);
			mfc.sendText(Constants.TXT_MOBILENUMBER, "9910767345", locatorType.ID);
			mfc.clickElementImage("SubmitButton.PNG");
			mfc.waitUntilFound(Constants.BUTTON_MUMBAI, locatorType.XPATH, 60);
			if(mfc.elementVisible(Constants.BUTTON_MUMBAI,locatorType.XPATH)) {
				mfc.passTest("Login Successfull", tcName);
			}else {
				mfc.failTest("Login Failed", tcName);
			}
		}catch(Exception e) {
			BaseSetup.logger.error(e);
		}
	}
	
	public void selectCity() {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();
		try {
			mfc.clickElement(Constants.BUTTON_MUMBAI, locatorType.XPATH);
			mfc.sendText(Constants.TXT_NEARESTLOC, "Mumbai", locatorType.ID);
			mfc.pressEnter();
			mfc.hideKeyboard();
			mfc.clickElement(Constants.BUTTON_SUBMIT_CITY,locatorType.ID);
			mfc.waitUntilFound(Constants.TXT_GENDER, locatorType.ID, 20);
			if(mfc.elementVisible(Constants.TXT_GENDER, locatorType.ID)) {
				mfc.passTest("City is Selected", tcName);
			}else {
				mfc.failTest("City is not Selected", tcName);
			}
		}catch(Exception e) {
			BaseSetup.logger.error(e);
		}
	}
	
	public void swipeScreen() {
		String tcName = new Throwable().getStackTrace()[0].getMethodName();
		try {
			mfc.getDeviceInfo();
			mfc.sleep(2);
			mfc.swipeScreen(Direction.UP);
			mfc.sleep(2);
			if(mfc.elementVisible(Constants.CHK_BANK, locatorType.XPATH)) {
				mfc.passTest("Scroll is working", tcName);
				mfc.clickElementPoint(mfc.createElement(Constants.CHK_BANK, locatorType.XPATH));
			}else {
				mfc.failTest("Scroll is not working", tcName);
			}
		}catch(Exception e) {
			BaseSetup.logger.error(e);
		}
	}

}
