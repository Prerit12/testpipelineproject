package com.core.base;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.core.utility.WebdriverListeners;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.core.utility.AppiumListener;
import com.core.utility.FunctionsClass;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.events.EventFiringWebDriverFactory;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseSetup {
	private static final String SOME_ERROR_OCCURED = "Some error occured";
	private static final String BROWSER = "Browser";

	public WebDriver driver;
	public AndroidDriver<MobileElement> androidDriver;
	public static AppiumDriverLocalService service;
	public static ExtentReports extent;
	public static ThreadLocal<ExtentTest> parentTest = new ThreadLocal<ExtentTest>();
	public static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();

	FunctionsClass fc = new FunctionsClass();

	String log4jConfPath = Constants.LOG4J_PATH;
	static String className = new Throwable().getStackTrace()[0].getClassName();
	public static final Logger logger = Logger.getLogger(className);

	/** Function to launch browser based on Excel Configuration */
	public WebDriver launchApplicationBrowser() {
		PropertyConfigurator.configure(log4jConfPath);
		logger.info("Launching the Application");
		try {
			if (fc.ExcelReader("AppType", "Web").get(0).contains("Yes")) {
				if (fc.ExcelReader(BROWSER, "Chrome").get(0).contains("Yes")) {
					launchChrome();
				} else if (fc.ExcelReader(BROWSER, "Firefox").get(0).contains("Yes")) {
					launchFireFox();
				} else if (fc.ExcelReader(BROWSER, "IE").get(0).contains("Yes")) {
					launchIE();
				} else {
					launchChrome();
				}
			} else {
				logger.warn("Please select App Type from Excel");
			}
		} catch (Exception e) {
			logger.error(SOME_ERROR_OCCURED + e);
		}

		EventFiringWebDriver eventDriver = new EventFiringWebDriver(driver);
		WebdriverListeners eCapture = new WebdriverListeners();
		eventDriver.register(eCapture);
		System.out.println("Driver is: " + eventDriver);
		return eventDriver;
	}

	/** Function to Launch Mobile Application based on Excel Configuration */
	public AndroidDriver<MobileElement> launchApplicationMobile() {
		try {
			if (fc.ExcelReader("AppType", "Mobile").get(0).contains("Yes")) {
				androidDriver = launchMobile();
			} else {
				logger.warn("Please select App Type from Excel");
			}
		} catch (Exception e) {
			logger.error(SOME_ERROR_OCCURED + e);
		}
//		nu.pattern.OpenCV.loadShared();
//		System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);
		androidDriver = EventFiringWebDriverFactory.getEventFiringWebDriver(androidDriver, new AppiumListener());
		return androidDriver;
	}

	/** Function to launch Chrome Browser */
	public WebDriver launchChrome() {
		try {
			logger.info("Chrome Browser is starting");
			WebDriverManager.chromedriver().setup();
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.addArguments("--headless");
			chromeOptions.addArguments("--no-sandbox");
			chromeOptions.addArguments("--disable-dev-shm-usage");
			driver = new ChromeDriver(chromeOptions);
		} catch (Exception e) {
			logger.error(SOME_ERROR_OCCURED + e);
		}
		return driver;
	}

	/** Function to launch Firefox Browser */
	public WebDriver launchFireFox() {
		try {
			logger.info("Firefox Browser is starting");
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		} catch (Exception e) {
			logger.error(SOME_ERROR_OCCURED + e);
		}
		return driver;
	}

	/** Function to launch IE browser */
	public WebDriver launchIE() {
		try {
			logger.info("IE Browser is starting");
			WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
		} catch (Exception e) {
			logger.error(SOME_ERROR_OCCURED + e);
		}
		return driver;
	}

	/** Function to Launch Mobile application */
	public AndroidDriver<MobileElement> launchMobile() {
		try {
			logger.info("Mobile Application is starting");
			AppiumServiceBuilder serviceBuilder = new AppiumServiceBuilder();
			service = AppiumDriverLocalService.buildService(serviceBuilder);
			service.start();

			DesiredCapabilities capabilities = new DesiredCapabilities();

			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, Constants.DEVICD_NAME);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Constants.PLATFORM_NAME);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, Constants.PLATFORM_VERSION);
			capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, Constants.APP_ACTIVITY);
			capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, Constants.APP_PACKAGE);
			//capabilities.setCapability("app", Constants.APP);
			androidDriver = new AndroidDriver<>(service.getUrl(), capabilities);
		} catch (Exception e) {
			logger.error(SOME_ERROR_OCCURED + e);
		}
		return androidDriver;
	}

}
