package com.core.base;

import org.openqa.selenium.By;

public class Constants {

	/** Device Capabilities */
	public static final String DEVICD_NAME = "Pixel 3 XL API 27";
	public static final String PLATFORM_NAME = "android";
	public static final String PLATFORM_VERSION = "8.1";
	public static final String APP = "./src/main/resources/in.workindia.apk";
	public static final String APP_NAME = "in.workindia.nileshdungarwal.workindiaandroid";
	// public static final String APP_ACTIVITY =
	// "in.workindia.nileshdungarwal.workindiaandroid.RegisterActivity";
	public static final String APP_ACTIVITY = "com.koushikdutta.vysor.StartActivity";
	// public static final String APP_PACKAGE =
	// "in.workindia.nileshdungarwal.workindiaandroid";
	public static final String APP_PACKAGE = "com.koushikdutta.vysor";

	/** Log4j path */
	public static final String LOG4J_PATH = "./src/main/resources/log4j.properties";

	/** Element Locator using By */
	public static final By elementID = By.id("id");
	public static final By elementXPath = By.xpath("//xpath");
	public static final By elementCSS = By.cssSelector("div span");
	public static final By elementName = By.name("name");
	public static final By elementTagname = By.tagName("tagname");
	public static final By elementLinkText = By.linkText("linkText");
	public static final By elementPartialLink = By.partialLinkText("partialLinkText");

	/** Directories path for Screenshots */
	public static final String PASS_TEST_CASE_FOLDER = "./Current_test_results/Reports/screenshots/PassedTestCaseFolder/";
	public static final String FAILED_TEST_CASE_FOLDER = "./Current_test_results/Reports/screenshots/FailedTestCaseFolder/";
	public static final String PASS_TEST_CASE_EXTENSION = "_Pass.png";
	public static final String FAILED_TEST_CASE_EXTENSION = "_Fail.png";
	public static final String VIDEO_FOLDER = "./Current_test_results/Reports/video/";

	/** Directories path for ExtentReports */
	public static final String EXTENT_REPORT_PATH = "./Current_test_results/Reports/ExtentReport.html";
	public static final String EXTENT_REPORT_CONFIG = "./src/main/resources/extent-config.xml";

	/** Google Page */
	public static final String TXT_SEARCH = "q";
	public static final String TXT_SEARCHED_TEXT = "//span[contains(text(),'Selenium')]";

	/** Calculator Page */
	public static final String BUTTON_NUM8 = "com.android.calculator2:id/digit_8";
	public static final String BUTTON_NUM7 = "com.android.calculator2:id/digit_7";
	public static final String BUTTON_ADD = "com.android.calculator2:id/op_add";
	public static final String BUTTON_SUM = "com.android.calculator2:id/result";

	/** WorkIndia Page */
	public static final String TXT_USERNAME = "in.workindia.nileshdungarwal.workindiaandroid:id/et_name";
	public static final String TXT_MOBILENUMBER = "in.workindia.nileshdungarwal.workindiaandroid:id/et_number";
	public static final String BUTTON_MUMBAI = "//android.widget.FrameLayout[1]/android.widget.Button[1]";
	public static final String BUTTON_SUBMIT = "in.workindia.nileshdungarwal.workindiaandroid:id/btn_submit";
	public static final String TXT_NEARESTLOC = "in.workindia.nileshdungarwal.workindiaandroid:id/act_auto_complete";
	public static final String BUTTON_SUBMIT_CITY = "in.workindia.nileshdungarwal.workindiaandroid:id/btn_done";
	public static final String TXT_GENDER = "in.workindia.nileshdungarwal.workindiaandroid:id/tv_my_gender";
	public static final String CHK_BANK = "//android.widget.FrameLayout[1]/android.widget.CheckBox[1]";

	/** Test Page */
	public static final String BTN_LOGIN = "(//a[contains(@class,'service-nav__login')])[1]";
	public static final String BTN_REGISTER = "loginSubmit";
	public static final String TXT_USERNAME_TEST = "loginfmt";
	public static final String BTN_NEXT = "//input[@type='submit']";
	public static final String TXT_PASSWORD = "passwd";
	public static final String BTN_CONTINUE = "//a[contains(@class,'banner-redirectlink')]";
}
